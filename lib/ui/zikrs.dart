import 'package:daily_zikr/bloc/add_zikr_bloc.dart';
import 'package:daily_zikr/bloc/bloc_provider.dart';
import 'package:daily_zikr/bloc/detail_zikr_bloc.dart';
import 'package:daily_zikr/bloc/zikrs_bloc.dart';
import 'package:daily_zikr/models/zikr.dart';
import 'package:daily_zikr/ui/add_zikr.dart';
import 'package:daily_zikr/ui/detail_zikr.dart';
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: BlocProvider<ZikrsBloc>(
        bloc: ZikrsBloc(),
        child: ZikrsPage(title: 'Зікір'),
      ),
    );
  }
}

class ZikrsPage extends StatelessWidget {
  ZikrsPage({Key key, this.title}) : super(key: key);
  final String title;

  ZikrsBloc bloc;

  @override
  Widget build(BuildContext context) {
    //todo Move to place where build will call only one time
    bloc = BlocProvider.of<ZikrsBloc>(context);
    bloc.fecthAllZiks();
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: StreamBuilder(
          stream: bloc.allZikrs,
          builder: (context, AsyncSnapshot<List<Zikr>> snapshot) {
            if (snapshot.hasData) {
              return buildZikrList(snapshot);
            } else if (snapshot.hasError) {
              return Text(snapshot.error.toString());
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          }),
      floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => BlocProvider<AddZikrBloc>(
                          bloc: AddZikrBloc(),
                          child: AddZikrPage((){ bloc.fecthAllZiks();}),
                        )));
          }),
    );
  }
}

handleListItemTap(BuildContext context, int zikrId){
  Navigator.push(context, MaterialPageRoute(
      builder: (context) =>
       BlocProvider<DetailZikrBloc>(
         bloc: DetailZikrBloc(), 
         child: DetailZikrPage(zikrId))
  ));
  //Open detail screen of zikr
  //Detail of zikr: 1. Name 2. Daily count 3. Place of tapping and increase count 4. Current count for today 5. Actions: remove, edit and other
}

Widget buildZikrList(AsyncSnapshot<List<Zikr>> snapshot) {
  return ListView.builder(
      itemCount: snapshot.data.length,
      itemBuilder: (context, index) {
        return ListTile(
          onTap:()=> handleListItemTap(context, snapshot.data[index].id),
          title: Text(snapshot.data[index].name),
          subtitle: Text(
              'Күнделікті жасалыну саны ${snapshot.data[index].dailyCount}'),
        );
      });
}
