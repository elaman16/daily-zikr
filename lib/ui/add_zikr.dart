import 'package:daily_zikr/bloc/add_zikr_bloc.dart';
import 'package:daily_zikr/bloc/bloc_provider.dart';
import 'package:flutter/material.dart';

class AddZikrPage extends StatelessWidget {
  AddZikrBloc addZikrBloc;
  Function success;

  AddZikrPage(this.success);

  @override
  Widget build(BuildContext context) {
    addZikrBloc = BlocProvider.of<AddZikrBloc>(context);
    addZikrBloc.updateList = (){
        success();
        Navigator.pop(context);
    };

    return Scaffold(
      appBar: AppBar(
        title: Text('Зікірді қосу'),
      ),
      body: Container(
        margin: EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            nameField(addZikrBloc),
            dailyCountField(addZikrBloc),
            Container(
                margin: EdgeInsets.only(top: 20.0),
                child: submitButton(addZikrBloc))
          ],
        ),
      ),
    );
  }

  Widget nameField(AddZikrBloc addZikrBloc) {
    return StreamBuilder(
      stream: addZikrBloc.name,
      builder: (context, snapshot) => TextField(
            onChanged: addZikrBloc.changeName,
            decoration:
                InputDecoration(labelText: 'Атауы', errorText: snapshot.error),
          ),
    );
  }

  Widget dailyCountField(AddZikrBloc addZikrBloc) {
    return StreamBuilder(
      stream: addZikrBloc.dailyCount,
      builder: (context, snapshot) => TextField(
            onChanged: addZikrBloc.changeDailyCount,
            decoration:
                InputDecoration(labelText: 'Күнделікті жасалыну саны', errorText: snapshot.error),
          ),
    );
  }

  Widget submitButton(AddZikrBloc addZikrBloc) {
    return StreamBuilder(
      stream: addZikrBloc.submitValid,
      builder: (context, snapshot) {
        if(!snapshot.hasData) return createButton(false, addZikrBloc); 
        else if(snapshot.data=='input') return createButton(true, addZikrBloc);
        else if(snapshot.data=='progress') return Center(child: CircularProgressIndicator(),);
      }
    );
  }
}

Widget createButton(bool activeButton, AddZikrBloc addZikrBloc){
  return RaisedButton(
          child: Text('Зікірді қосу'),
          color: Colors.blue[700],
          textColor: Colors.white,
          onPressed: activeButton? addZikrBloc.submit : null,
        );
}
