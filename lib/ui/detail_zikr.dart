import 'package:daily_zikr/bloc/bloc_provider.dart';
import 'package:daily_zikr/bloc/detail_zikr_bloc.dart';
import 'package:daily_zikr/models/zikr.dart';
import 'package:flutter/material.dart';

DetailZikrBloc bloc;

class DetailZikrPage extends StatelessWidget {
  num zikrId;

  DetailZikrPage(this.zikrId);

  @override
  Widget build(BuildContext context) {
    bloc = BlocProvider.of<DetailZikrBloc>(context);
    bloc.getDetailZikr(zikrId);

    return Scaffold(
      appBar: AppBar(
        title: Text("Зікір"),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 30.0,
            ),
            zikrName(),
            SizedBox(
              height: 20.0,
            ),
            zikrDailyCount(),
            SizedBox(
              height: 20.0,
            ),
            zikrIncreaser(zikrId)
          ],
        ),
      ),
    );
  }
}

Widget zikrName() {
  return StreamBuilder(
      stream: bloc.detailZikr,
      builder: (context, AsyncSnapshot<Zikr> snapshot) => Text(
        'Зікірдің атауы: ${snapshot.data.name}',
        style: TextStyle(fontSize: 18.0,),
      ));
}

zikrDailyCount() {
  return StreamBuilder(
    stream: bloc.detailZikr,
    builder: (context, AsyncSnapshot<Zikr> snapshot) => Text(
          'Зікірдің күнделікті жасалыну керек саны: ${snapshot.data.dailyCount.toString()}',
          style: TextStyle(fontSize: 17.0),
        ),
  );
}

zikrIncreaser(zikrId) {
  return Expanded(
      child: GestureDetector(
        onTap: () => bloc.increaser(zikrId),
              child: Container(
        color: Colors.grey[300],
        margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
        child: Center(
          child: StreamBuilder(
              stream: bloc.zikrCount,
              builder: (context, AsyncSnapshot<int> snapshot) => 
              Text(
              '${snapshot.data}', 
              style: TextStyle(fontSize: 27.0),
            ),
          ),
        ),
    ),
      ),
  );
}
