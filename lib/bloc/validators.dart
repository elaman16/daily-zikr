import 'dart:async';

class Validators {
  final validateName =
      StreamTransformer<String, String>.fromHandlers(handleData: (text, sink) {
    if (text.isEmpty) {
      sink.addError('Бос болмауы керек');
    } else {
      sink.add(text);
    }
  });

  final validateDailyCount =
      StreamTransformer<String, String>.fromHandlers(handleData: (text, sink) {

    RegExp regExp = new RegExp(r"^[0-9]+$");   
    if (text.isEmpty) {
      sink.addError('Бос болмауы керек');
    } else if(regExp.hasMatch(text)){
      sink.add(text);
      } else {
       sink.addError('Тек сан ғана енгізіңіз');
    }
  });
}
