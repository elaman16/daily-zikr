
import 'package:daily_zikr/bloc/bloc_provider.dart';
import 'package:daily_zikr/models/zikr.dart';
import 'package:daily_zikr/resourses/DataBase.dart';
import 'package:rxdart/rxdart.dart';

class ZikrsBloc implements BlocBase{
  final _zikrsFetcher  =  BehaviorSubject<List<Zikr>>();

  Observable<List<Zikr>> get allZikrs => _zikrsFetcher.stream;

  void fecthAllZiks() async{
     List<Zikr> items = List<Zikr>();
     items = await DBProvider.db.getAllZikrs();
    _zikrsFetcher.sink.add(items);
  }

  dispose(){
    _zikrsFetcher.close();
  }
}