import 'package:daily_zikr/bloc/bloc_provider.dart';
import 'package:daily_zikr/bloc/validators.dart';
import 'package:daily_zikr/models/zikr.dart';
import 'package:daily_zikr/resourses/DataBase.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class AddZikrBloc extends Object with Validators implements BlocBase{
  Function updateList;

  final _name = BehaviorSubject<String>();
  final _dailyCount = BehaviorSubject<String>();

  Stream<String> get name =>  _name.stream.transform(validateName);
  Stream<String> get dailyCount =>  _dailyCount.stream.transform(validateDailyCount);
  Stream<String> get submitValid => Observable.combineLatest2(name, dailyCount, (n, d) => "input");

  Function(String) get changeName => _name.sink.add;
  Function(String) get changeDailyCount => _dailyCount.sink.add;


  submit() async{
    final validName  = _name.value;
    final validDailyCount = _dailyCount.value;
    print('$validName and $validDailyCount');
    await DBProvider.db.newZikr(Zikr(name: _name.value, dailyCount: int.parse(_dailyCount.value)));
    updateList();
  }

  dispose(){
    _name.close();
    _dailyCount.close();
  }

  void close() {}
}