import 'package:daily_zikr/bloc/bloc_provider.dart';
import 'package:daily_zikr/models/zikr.dart';
import 'package:daily_zikr/models/zikr_history.dart';
import 'package:daily_zikr/resourses/DataBase.dart';
import 'package:rxdart/rxdart.dart';

class DetailZikrBloc extends BlocBase {
  final _detailZikrFetcher = BehaviorSubject<Zikr>();
  final _zikrCountFetcher = BehaviorSubject<int>();

  Observable<Zikr> get detailZikr => _detailZikrFetcher.stream;
  Observable<int> get zikrCount => _zikrCountFetcher.stream;

  void getDetailZikr(id) async {
    Zikr zikr = await DBProvider.db.getZikr(id);
    _detailZikrFetcher.sink.add(zikr);

    ZikrHistory zikrHistory = await DBProvider.db.getLastHistory(id);
    _zikrCountFetcher.sink.add(zikrHistory!= null? zikrHistory.count: 0);
  }

  increaser(zikrId) async {
//User tab increase button ->
//1. get last row 'date'
//2. check with 'current_date'
//3. if they are equal get this row 'counter' else create a new
//row with zero 'counter' value
//4. Increase counter to one and write down again to db
//5. Notify view

    String lastDate;
    int counter = 0;

    var lastZikrHistory = await DBProvider.db.getLastHistory(zikrId);
    if(lastZikrHistory!=null)
    lastDate = lastZikrHistory.date;

    DateTime today = new DateTime.now();
    String todayFormatted = '${today.day}-${today.month}-${today.year}';

    if (lastDate != null && lastDate.isNotEmpty && lastDate == todayFormatted) {
      //Get count from last row
      counter = lastZikrHistory.count;
      //Increase count to one
      counter++;
      //Insert count again to last row
      await DBProvider.db.updateDailyCount(lastZikrHistory.id, counter);
    } else {
      //Increase count to one
      counter++;

      //Create a new row
      //Insert current data and count to new created row
      await DBProvider.db.newDailyCount(ZikrHistory(date: todayFormatted, count: counter, zikrId: zikrId));
    }

    _zikrCountFetcher.sink.add(counter);
  }

  @override
  void dispose() {
    _detailZikrFetcher.close();
    _zikrCountFetcher.close();
  }
}
