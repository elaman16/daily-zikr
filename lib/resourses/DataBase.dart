import 'dart:io';

import 'package:daily_zikr/models/zikr.dart';
import 'package:daily_zikr/models/zikr_history.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';


class DBProvider{
  String ZIKR_TABLE = 'Zikr';
  String ZIKR_HISTORY_TABLE = 'ZikrHistories';

  DBProvider._();
  static final DBProvider db = DBProvider._();

  static Database _dataBase;

  Future<Database> get  database async {
    if(_dataBase != null){
      return _dataBase;
    }

    _dataBase = await initDB();
    return _dataBase;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'Zikr.db');
    return await openDatabase(path, version: 1,
        onOpen: (db) {},
        onCreate: (Database db, int version) async {
          await db.execute("CREATE TABLE $ZIKR_TABLE ("
              "id INTEGER PRIMARY KEY,"
              "name TEXT,"
              "daily_count INTEGER"
              ")");

          await db.execute("CREATE TABLE $ZIKR_HISTORY_TABLE ("
              "id INTEGER PRIMARY KEY, "
              "date TEXT,"
              "count INTEGER,"
              "zikr_id INTEGER, "
              "FOREIGN KEY(zikr_id) REFERENCES $ZIKR_TABLE(id)"
              ")"); 
        });
  }

  newZikr(Zikr zikr) async{
    final db = await database;
    var raw = await db.rawInsert(
      "INSERT Into $ZIKR_TABLE (name, daily_count)"
          "VALUES (?, ?)",
      [zikr.name, zikr.dailyCount]
    );
    return raw;
  } 

   newDailyCount(ZikrHistory zikrHistory) async{
    final db = await database;
    var raw = await db.rawInsert(
      "INSERT Into $ZIKR_HISTORY_TABLE (date, count, zikr_id)"
      "VALUES (?, ?, ?)",
      [zikrHistory.date, zikrHistory.count, zikrHistory.zikrId]
    );
    return raw;
  }

  getZikr(int id) async{
    final db = await database;
    var res = await db.query("$ZIKR_TABLE", where: "id= ?", whereArgs: [id]);
    return res.isNotEmpty ? Zikr.fromJson(res.first): Null;
  }

  getAllZikrs() async{
    final db = await database;
    var res = await db.query("$ZIKR_TABLE");
    List<Zikr> list = 
        res.isNotEmpty ? res.map((zikr) => Zikr.fromJson(zikr)).toList() : [];
    return list;
  }

  Future<ZikrHistory> getLastHistory(int zikrId) async {
    //TODO make by sql
    final db = await database;
    var res = await db.query("$ZIKR_HISTORY_TABLE", where: "zikr_id = ?", whereArgs: [zikrId]);
    return res.isNotEmpty ? ZikrHistory.fromJson(res.last): null;
  }

  updateDailyCount(int zikrHistoryId, int counter) async{
    final db = await database;
    // return await db.query("UPDATE $ZIKR_HISTORY_TABLE "
    // "SET count=$counter "
    // "WHERE id=$zikrHistoryId");

    int count = await db.rawUpdate('UPDATE $ZIKR_HISTORY_TABLE SET count=? WHERE zikr_id=? ',
    [counter, zikrHistoryId]);
    print('updated: $count');
  }

}

