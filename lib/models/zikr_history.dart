class ZikrHistory{
  int id;
  String date;
  int count;
  int zikrId;

  ZikrHistory({this.id, this.date, this.count, this.zikrId});

  ZikrHistory factory ZikrHistory.fromJson(Map<String, dynamic> json)=> new ZikrHistory(
    id: json['id'], 
    date: json['date'],
    count: json['count'],
   zikrId: json[ 'zikr_id']
  );
}