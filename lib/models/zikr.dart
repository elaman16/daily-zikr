import 'dart:convert';

Zikr zikrFromJson(String str) {
  final jsonData = json.decode(str);
  return Zikr.fromJson(jsonData);
}

String zikrToJson(Zikr data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class Zikr {
  int id;
  String name;
  int dailyCount;

  Zikr({
    this.id,
    this.name,
    this.dailyCount,
  });

  factory Zikr.fromJson(Map<String, dynamic> json) => new Zikr(
    id: json["id"],
    name: json["name"],
    dailyCount: json["daily_count"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "daily_count": dailyCount,
  };
}